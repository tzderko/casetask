/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import submissionList  from '@salesforce/apex/SubmissionComponentController.getSubmissionList';
import submissionFields from '@salesforce/apex/SubmissionComponentController.getSubmissionFields';

export default class SubmissionComponent extends LightningElement {
    
    @api oppid;
    @track showNewSubmissionModal = false;
    @track submissions=[]; 
    
    fields = [];
    columnsHeader = [];

    constructor(){
        super();
        submissionFields({objName: 'BG_Submission__c', fieldSetApiName: 'GRIDSubmissionFieldSet'}).then((result) => {
            // console.log('result lwc', result);
            for(let f of Object.entries(result)){
                this.fields.push(f[1]);
                this.columnsHeader.push(f[0]);    
            }
        }).then(() => {
            submissionList({oppId: this.oppid, fields: this.fields}).then((result) => {
                // console.log('result lwc', result);
                this.submissions = result;
            });      
        });
    }



    // connectedCallback(){
    //     console.log('this.fields', this.fields);
    //     submissionList({oppId: this.oppid, fields: this.fields}).then((result) => {
    //         this.submissions = result;
    //     });      
    // }

    // @wire(submissionFields, {objName: 'BG_Submission__c', fieldSetApiName: 'GRIDSubmissionFieldSet'})
    // wiredfields(data,error){
    //     if(data){
    //         console.log('data',data);
    //     }
    //     if(error){
    //         console.log('fieldss','errrrrrr');
    //     }
    // }

    // connectedCallback(){

    // }


    // @wire(submissionList, {oppId:'$oppid'})
    // wiredsubmissions(data, error){
    //     if(data){
    //         console.log('data', data);
    //     }
    //     if(error){
    //         console.log('fieldss','errrrrrr');
    //     }
    // }
    

    /*wiredfields({error, data}){
        if(data){
            this.fields = JSON.parse(JSON.stringify(data));
            // eslint-disable-next-line no-console
            console.log('fieldss',JSON.parse(JSON.stringify(this.fields)));
        }
        if(error){
            this.error = error;
        }
    }*/


    newSubmission(){
        // eslint-disable-next-line no-console
        console.log('new subm click');
        console.log(this.submissions);
        this.showNewSubmissionModal = true;
    }

    submitToBank(){
        
        console.log('submit to bank');
    }

    openmodal() {
        this.showNewSubmissionModal = true
    }
    closeModal() {
        this.showNewSubmissionModal = false
    } 
    saveMethod() {
         // eslint-disable-next-line no-console
        console.log('save on modal');
        this.closeModal();
    }

   
}