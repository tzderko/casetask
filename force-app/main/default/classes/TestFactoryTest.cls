@isTest
private class TestFactoryTest{
    
    @isTest
    static void whenObjectIsCreatedExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account testAccount = (Account)TestFactory.createSObject(new Account());
        Test.stopTest();
        System.assertEquals('Test Account', testAccount.Name, 'Expecting Default field value is set to specific value');
    }
    
    @isTest
    static void whenObjectIsInsertedExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account testAccount = (Account)TestFactory.createSObject(new Account(),true);
        Test.stopTest();
        testAccount = [SELECT Name FROM Account WHERE Id = :testAccount.Id];
        System.assertEquals('Test Account', testAccount.Name, 'Expecting Default field value is set to specific value');
    }
    
    @isTest
    static void whenObjectIsCreatedWithSpecificDefaultsSetExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account testAccount = (Account)TestFactory.createSObject(new Account(), 'TestFactoryDefaults.MyAccountDefaults');
        Test.stopTest();
        System.assertEquals('My Test Account', testAccount.Name, 'Expecting Default field value is set to specific value');
    }
    
    @isTest
    static void whenObjectIsInsertedWithSpecificDefaultsSetExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account testAccount = (Account)TestFactory.createSObject(new Account(), 'TestFactoryDefaults.MyAccountDefaults', true);
        Test.stopTest();
        System.assertEquals('My Test Account', testAccount.Name, 'Expecting Default field value is set to specific value');
    }
    
    @isTest
    static void whenListOfObjectsIsCreatedExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account[] testAccounts = (Account[])TestFactory.createSObjectList(new Account(), 200);
        Test.stopTest();
        System.assertEquals(200, testAccounts.size());
        System.assertEquals('Test Account 0', testAccounts[0].Name);
        System.assertEquals('Test Account 199', testAccounts[199].Name);
        for(Account testAccount : testAccounts){
            System.assert(testAccount.Name.startsWith('Test Account'), 'Expecting Default field value is set to specific value');
        }
    }
    
    @isTest
    static void whenListOfObjectIsInsertedExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account[] testAccounts = (Account[])TestFactory.createSObjectList(new Account(), 200, true);
        Test.stopTest();
        testAccounts = [SELECT Name FROM Account WHERE Id IN :testAccounts ORDER BY Name];
        System.assertEquals(200, testAccounts.size());
        System.assertEquals('Test Account 0', testAccounts[0].Name);
        for(Account testAccount : testAccounts){
            System.assert(testAccount.Name.startsWith('Test Account'), 'Expecting Default field value is set to specific value');
        }
    }
    
    @isTest
    static void whenListOfObjectsIsCreatedWithSpecificDefaultsSetExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account[] testAccounts = (Account[])TestFactory.createSObjectList(new Account(), 200, 'TestFactoryDefaults.MyAccountDefaults');
        Test.stopTest();
        System.assertEquals(200, testAccounts.size());
        System.assertEquals('My Test Account 0', testAccounts[0].Name);
        System.assertEquals('My Test Account 199', testAccounts[199].Name);
        for(Account testAccount : testAccounts){
            System.assert(testAccount.Name.startsWith('My Test Account'), 'Expecting Default field value is set to specific value');
        }
    }
    
    @isTest
    static void whenListOfObjectsIsInsertedWithSpecificDefaultsSetExpectDefaultFieldsArePopulated(){
        Test.startTest();
        Account[] testAccounts = (Account[])TestFactory.createSObjectList(new Account(), 200, 'TestFactoryDefaults.MyAccountDefaults', true);
        Test.stopTest();
        testAccounts = [SELECT Name FROM Account WHERE Id IN :testAccounts ORDER BY Name];
        System.assertEquals(200, testAccounts.size());
        System.assertEquals('My Test Account 0', testAccounts[0].Name);
        for(Account testAccount : testAccounts){
            System.assert(testAccount.Name.startsWith('My Test Account'), 'Expecting Default field value is set to specific value');
        }
    }
}