/**
 * @File Name          : CaseSearchController.cls
 * @Description        : Used in CaseComponentMain and CasetableComponent.
 * @Author             : Taras Zderko	
 * @Last Modified On   : 12/11/2019, 11:52:38 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019        Taras Zderko			   Initial Version
**/
public with sharing class CaseSearchController {
    
 	private static ApexPages.StandardSetController ssc;
    
    /**
    * @description : Used to get all unique accounts and status from Case to fill in data for available filters on the page
    * @author      : Taras Zderko | 12/11/2019 
    * @param       : searchField - field for AggregateResult function, will get all unique Case Status and Account Name
    * @return      : List<String>- unique status and Account Names
    **/
    @AuraEnabled
    public static List<String> uniqueCaseFilterData(String searchField){
        switch on searchField{
            when 'Status' {
                return queryData('Status', 'Status');
            }
            when 'Account'{
                return queryData('Account.Name', 'Name');
            }
            when else {
                return new List<String>();
            }
        }
    }

    /**
    * @description : Case_Column__mdt describe colunms and all fields taht we need to dinamicly generate datatable. 
                     Controll what fields will be available for sorting, colums label, filed type etc.
    * @author      : Taras Zderko | 12/11/2019 
    * @return      : List<Case_Column__mdt> 
    **/
    @AuraEnabled
    public static List<Case_Column__mdt> queryColumns(){
        SObjectType mdType = Schema.getGlobalDescribe().get('Case_Column__mdt');
		Map<String,Schema.SObjectField> mfields = mdType.getDescribe().fields.getMap();
        String query = 'SELECT ' +String.join(new List<String>(mfields.keySet()), ', ') +' FROM Case_Column__mdt';
        return Database.query(query);
    }
    
    /**
    * @description : Query cases depends on filters, page number, order
    * @author      : Taras Zderko | 12/11/2019 
    * @params      : fields - API names of fields for query from Case_Column__mdt
                   : status - case status from selected filter
                   : accountName - selected account name from selected filter
                   : description - typed description for case
                   : pageSize - how many records display on one page(default 15, defined in component)
                   : pageNumber - for offset
                   : sortBy - field for sorting 
                   : sortOrder - order (default asc, defined in component)
    * @return ResultWrapper 
    **/
    @AuraEnabled
    public static ResultWrapper searchCase(List<String> fields, String status, String accountName, String description, 
                                           Integer pageSize, Integer pageNumber, String sortBy, String sortOrder){
                                               
        Map<String, String> filetedMap = New Map<String, String>();
        filetedMap.putAll(validateInput('Status', status));
        filetedMap.putAll(validateInput('Account.Name',accountName));
        
        String query = buildQuery(fields, filetedMap, sortby, sortOrder);
       
        ssc = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        ssc.setpagesize(pageSize);
        ssc.setPageNumber(pageNumber);
        
        //  this part present because of description field, it cant be sorted in query 
        if(description != null && description != ''){
            List<Case> caseList = new List<Case>();
            for(Case c : (List<Case>)ssc.getRecords()){
                if(c.Description != null && c.Description.containsIgnoreCase(description)){
                    caseList.add(c);
                }
            }
            return new ResultWrapper(caseList, caseList.size());
        // ----------------------------------------------------------------------------
        }else{
           return new ResultWrapper((List<Case>)ssc.getRecords(), ssc.getResultSize());
        }
    }

    /**
    * @description : Close cases
    * @author      : Taras Zderko  | 12/11/2019 
    * @param       : ids  - selected record ids to close cases.  
    * @return      : void 
    **/
    @AuraEnabled
    public static void updateCases(List<String> ids){
        List<Case> casesToUpdate = new List<Case>();
        for(String iter : ids){
            if(iter instanceOf ID){
                casesToUpdate.add(new Case(id = iter, Status = 'Closed'));
            }
        }
        update casesToUpdate;
    }
     
    /**
    * @description : private help method for quering data
    * @author      : Taras Zderko | 12/11/2019 
    * @params      : searchField - field for aggreagate
                   : getByName   - key word to get aggregated values
    * @return      : List<String>- pupulated data
    **/
    private static List<String> queryData(String searchField, String getByName){
        List<String> tempData = new List<String>();
        for(AggregateResult res : Database.query('Select ' +  searchField +' FROM Case Where Status != \'Closed\' GROUP BY ' + searchField)){
            tempData.add((String)res.get(getByName));
        }
        return tempData;
    }

    /**
    * @description : validate input fields
    * @author      : Taras Zderko | 12/11/2019 
    * @params      : APINameField - Field API name for query
                   : inputField - value for current field 
    * @return Map<String, String> 
    **/
    private static Map<String, String> validateInput(String APINameField, String inputField){
        Map<String, String> returnDataMap = new Map<String, String>();
        if(inputField != null){
            returnDataMap.put(APINameField, String.escapeSingleQuotes(inputField));
        }
        return returnDataMap;
    }
    
    /**
    * @description : create query. Case id hardcoded because it needed anyway, same with status.
    * @author      : Taras Zderko | 12/11/2019 
    * @params      : fields - all Case fields from Metadata 
                   : queryParams - validated input for filters
                   : sortBy - sorting column
                   : sortOrder - sorting order
    * @return      : String - ready query string with filters, and sorting
    **/
    private static String buildQuery(List<String> fields, Map<String, String> queryParams, String sortBy, String sortOrder){
        String query = 'SELECT id, ' +String.join(fields, ', ') +' FROM Case WHERE Status != \'Closed\'';
        Integer loopCount = 0;
        if(queryParams.keySet().size() > 0 ){
            query += ' AND ';
            for(String iter : queryParams.keySet()){
                query += iter + ' = \'' + queryParams.get(iter) +  '\' ';
                loopCount++;
                if(loopCount < queryParams.keySet().size()){query += ' AND '; }
            }
        }
        if(sortby != NULL && sortOrder != NULL){
            query += ' ORDER BY '+ sortby+' '+ sortOrder+' NULLS LAST ';
        }
        return query;
    }
    
   
    
    /**
    * @description : Close cases
    * @author      : Taras Zderko  | 12/11/2019 
    * @param       : result  - List of all cases depends on filter and page size 
                   : totalNumberOfRecords - number of all cases for selecterd filter. Need this for correct parination calcs.
    **/
    public class ResultWrapper {
        @AuraEnabled public List<Case> result {get;set;}
        @AuraEnabled public Integer totalNumberOfRecords {get;set;}
        
        public ResultWrapper(List<Case> result, Integer totalNumberOfRecords) {
            this.result = result;
            this.totalNumberOfRecords = totalNumberOfRecords;
        }
    }
}