/**
 * @File Name          : Utils.cls
 * @Description        : Usefull Util class
 * @Author             : Taras Zderko
 * @Last Modified By   : 
 * @Last Modified On   : 12/16/2019, 6:03:24 PM
 * Ver       Date            Author      		    Modification
 * 1.0    12/16/2019        Taras Zderko           Initial Version
**/
public with sharing class Utils {

    static Integer[] charset;
    static {
        charset = new Integer[0];
        for(Integer i = 48; i < 58; i++) charset.add(i);
        for(Integer i = 65; i < 91; i++) charset.add(i);
        for(Integer i = 97; i < 123; i++) charset.add(i);
    }
    
    /**
        * @description : Generate random String 
        * @params      : len - generated String length 
        * @return String 
    **/
    public static String genRndStrFast(Integer len) {
        Integer[] chars = new Integer[len];
        Integer key, size = charset.size();
        for(Integer idx = 0; idx < len; idx++) {
            chars[idx] = charset[Math.mod(Math.abs(Crypto.getRandomInteger()), size)];
        }
        return String.fromCharArray(chars);
    }

    public static Integer getFieldLength( SObjectField field ){
        return field.getDescribe().getLength();
    }
    
    /**
        * @description : Method to get Set of Ids from list SObject records
        * @params      : records - list of records to get ids 
                       : field - field 
        * @return Set<Id> 
    **/
    public static Set<String> filterIds(List<SObject> records, Schema.sObjectField field) {
        Set<String> toReturn = new Set<String>();
        for(SObject record : records ){
            if (record.get(field) != null) {
                toReturn.add(String.valueOf(field));
            }
        }
        return toReturn;
    }

    /**
        * @description : Work with sObject List. 
        * @params      : records - List of SObjects 
                       : key - Map Key for grouping
        * @return Map<String, List<sObject>> 
    **/
    public static Map<String, List<sObject>> createKeyToListMap(List<SObject> records, String key){
        Map<String,List<SObject>> toReturn = new Map<String,List<SObject>>();
        for(SObject obj : records){
            if(toReturn.containsKey((String)obj.get(key))){
                toReturn.get((String)obj.get(key)).add(obj);
            }else {
                toReturn.put(key, new List<SObject>{obj});
            }
        }
        return toReturn;
    }

    /**
        * @description : Retrieves Picklist Entries for specified field
        * @params      : obj - object type
                       : field - picklist field
        * @return Picklist Entries
    */
    public static Schema.PicklistEntry[] getPicklistValues( Schema.SObjectType obj, String field ) {
        Map<String,SObjectField> fieldResultMap = obj.getDescribe().fields.getMap();
        return fieldResultMap.get(field).getDescribe().getPicklistValues();
    }

    /**
        * @description : Retrieves map of picklist values to labels
        * @params      : obj - SObject type
                       : field - SObject field
        * @return Map of picklist values to labels
    */
    public static Map<String, String> getPicklistValuesMap( Schema.SObjectType obj, String field ) {
        Map<String, String> picklistValuesMap = new Map<String, String>();
        for( Schema.PicklistEntry ple : getPicklistValues( obj, field ) ){
            picklistValuesMap.put( ple.getValue(), ple.getLabel() );
        }
        return picklistValuesMap;
    }
    
    /**
        * @description : Get Record Type id 
        * @params      : objectName - API name of object
                       : strRecordTypeName - RecordType Name
        * @return Id 
    **/
    public static Id getRecordTypeIdbyName(String objectName, String strRecordTypeName){
        return  Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
    } 

    /**
        * @description : Create Exceptions
        * @author      : Taras Zderko | 12/16/2019 
        * @param       : e -  Exception type
        * @return AuraHandledException 
    **/
    public static AuraHandledException createAuraException( Exception e ) {
        String exMessage = e.getMessage();
        return createAuraException( exMessage );
    }

    public static AuraHandledException createAuraException( String exMessage ) {
        AuraHandledException ex = new AuraHandledException( exMessage );
        ex.setMessage( exMessage );
        return ex;
    }

    /**
        * @description : Get return picklist values depends on Record type. 
        * @author      : Taras Zderko | 12/16/2019 
        * @params      : objectType 
                       : recordTypeId 
                       : fieldName 
        * @return Map<String, String> 
    **/  
    // TODO: In case of usage create Named Creds and update httpReq.setEndpoint()!
     public static Map<String, String> getPicklistValuesForRecordType(String objectType, String recordTypeId, String fieldName) {
        //Endpoint
        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setMethod('GET');
        String endpoint = String.format('/services/data/v41.0/ui-api/object-info/{0}/picklist-values/{1}/{2}', new String[]{ objectType, recordTypeId, fieldName});
        httpReq.setEndpoint('callout:REST_API1' + endpoint);
        HttpResponse res = h.send(httpReq);
        //Parse response
        Map<String, String> result = new Map<String, String>();
        Map<String, Object> root = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        if (!root.containsKey('values')) {
            return result;
        }
        List<Object> pValues = (List<Object>) root.get('values');
        for (Object pValue : pValues) {
            Map<String, Object> pValueMap = (Map<String, Object>) pValue;
            result.put((String) pValueMap.get('value'), (String) pValueMap.get('label'));
        }
        return result;
    }    
}
