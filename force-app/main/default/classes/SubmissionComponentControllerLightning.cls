public with sharing class SubmissionComponentControllerLightning {

    @AuraEnabled
    public static List<BG_Submission__c> getSubmissionList(String oppId, List<String> fields){
        String query = 'SELECT ' +String.join(fields, ', ') +' FROM BG_Submission__c WHERE  Opportunity__c =: oppId  Order By Name ASC';
        return Database.query(query);
    }
    
    @AuraEnabled
    public static List<FieldsWrapper> getSubmissionFields(String objName, String fieldSetApiName){
        List<FieldsWrapper> returnFieldsList = new List<FieldsWrapper>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);

        // SObjectType r = ((SObject)(Type.forName('Schema.'+objName).newInstance())).getSObjectType();
        // JSON.serialize(r.getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().getType()))

        for(Schema.FieldSetMember f : fieldSetObj.getFields()){
            returnFieldsList.add(new FieldsWrapper(f.getLabel(), f.getFieldPath()));                                                  
        }   
        return returnFieldsList;
    }

    @AuraEnabled
    public static String deleteSubmission(String recordToDelete){
       Database.DeleteResult deleteResults = Database.delete(recordToDelete, false);
        if(!deleteResults.isSuccess()) {
            return 'The following error has occurred. ';
        }
       return '';    
    }


    public class FieldsWrapper {
        @AuraEnabled public String fieldLabel;
        @AuraEnabled public String fieldAPIName;
        @AuraEnabled public String fieldType;

        public FieldsWrapper(String label, String APIName){
            this.fieldLabel = label;
            this.fieldAPIName = APIName;
            this.fieldType = 'text';
        }
    }
}