/**
 * @File Name          : TestFactoryDefaults.cls
 * @Description        : The TestFactory will pre-fill all the fields we typically need
	                     Account a = (Account)TestFactory.createSObject(new Account());
	                     insert a;

	                     You can also set values to be used. Any values set in the constructor will override the defaults
	                     Opportunity o = (Opportunity)TestFactory.createSObject(new Opportunity(AccountId = a.Id));

	                     You can also specify a specific set of overrides for different scenarios
	                     Account a = (Account)TestFactory.createSObject(new Account(), 'AccountDefaults');

	                     Finally, get a bunch of records for testing bulk
	                     Account[] aList = (Account[])TestFactory.createSObjectList(new Account(), 200);

	                     You can optionally insert records as created. Note the final parameter of true.
	                     Account a = (Account) TestFactory.createSObject(new Account(), true);
	                     Contact c = (Contact) TestFactory.createSObject(new Contact(AccountID = a.Id), true);

                         To specify defaults for objects, use the naming convention [ObjectName]Defaults.
                         For custom objects, omit the __c from the Object Name
 * @Author             : 
 * @Last Modified By   : 
 * @Last Modified On   : 12/16/2019, 1:59:31 PM
 * Ver       Date        Author      		Modification
 * 1.0    12/5/2019                       Initial Version
**/

@isTest
public class TestFactoryDefaults{
    
    public class UserDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            String lastName = 'Tester';
            Integer uniqueInteger = System.now().millisecond();
            return new Map<Schema.SObjectField, Object> {
                User.FirstName         => 'Inigo Montoya',
                User.LastName          => lastName,
                User.Alias             => EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf(lastName + uniqueInteger))).substring(0,8),
                User.Email             => lastName + '.' + uniqueInteger + '.' + UserInfo.getOrganizationId() + '@example.com',
                User.Username          => lastName + '.' + uniqueInteger + '@example.com.' + UserInfo.getOrganizationId(),
                User.ProfileId         => [SELECT Id FROM Profile WHERE Name  = 'System Administrator' LIMIT 1].Id,
                // User.UserRoleId     => [SELECT Id FROM UserRole WHERE Name = 'CEO' LIMIT 1].Id,
                User.TimeZoneSidKey    => 'America/Chicago',
                User.LanguageLocaleKey => 'en_US',
                User.EmailEncodingKey  => 'UTF-8',
                User.LocaleSidKey      => 'en_US'
            };
       }
    }
    
    public class AccountDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Account.Name => 'Test Account'
            };
        }
    }
    
    public class MyAccountDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Account.Name => 'My Test Account'
            };
        }
    }
    
    public class ContactDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Contact.FirstName => 'First',
                Contact.LastName  => 'Last'
            };
        }
    }
    
    public class OpportunityDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Opportunity.Name      => 'Test Opportunity',
                Opportunity.StageName => 'Closed Won',
                Opportunity.CloseDate => System.today()
            };
        }
    }
    
    public class CaseDefaults implements TestFactory.FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Case.Subject => 'Test Case'
            };
        }
    }
}