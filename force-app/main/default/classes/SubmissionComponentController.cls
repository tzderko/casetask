public with sharing class SubmissionComponentController {

    @AuraEnabled(cacheable=false)
    public static List<BG_Submission__c> getSubmissionList(String oppId, List<String> fields){
        String query = 'SELECT ' +String.join(fields, ', ') +' FROM BG_Submission__c WHERE  Opportunity__c =: oppId  Order By Name ASC';
        return Database.query(query);
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String, String> getSubmissionFields(String objName, String fieldSetApiName){
        Map<String, String> returnFieldsList = new Map<String, String>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);
        for(Schema.FieldSetMember f : fieldSetObj.getFields()){
            returnFieldsList.put(f.getLabel() ,f.getFieldPath());
        }
        return returnFieldsList;
    }
}