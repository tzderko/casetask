({
    lazyLoadTabs: function (component, event) {
        const recordId = component.get('v.recordId');
        var tab = event.getSource();
        switch (tab.get('v.id')) {
            case 'submissions':
                this.injectComponent('c:submissionLightningComponent', tab, recordId, 'BG_Submission__c', 'GRIDSubmissionFieldSet');
                break;
            case 'offers':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'stips':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'balances':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'sHistoryes':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'commissions':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'syndications':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'cRoles':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'tasks':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
            case 'attachments':
                //this.injectComponent('c:myCaseComponent', tab);
                break;
        }
    },

    injectComponent: function (name, target, recordId, objAPIName, fieldSetApiname) {
        $A.createComponent(name, { 'oppId': recordId, 'objApiName' : objAPIName,'fieldSet': fieldSetApiname}, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else if (status === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message: error.message,
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
            }
        });
    }
})