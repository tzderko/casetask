({
    prepareColumns: function (component) {
        const actions = [
            { label: 'Edit', name: 'edit' },
            { label: 'Delete', name: 'delete' },
            { label: 'Submit to Bank', name: 'sendToBank' }
        ];
        var fields = [];
        var columns = [];

        var action = component.get("c.getSubmissionFields");
        action.setParams({
            objName: component.get('v.objApiName'),
            fieldSetApiName: component.get('v.fieldSet')
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respData = response.getReturnValue();
                for (let f of Object.entries(respData)) {
                    console.log('f', f);
                    if (f[1].fieldAPIName != 'Id') {
                        columns.push({ label: f[1].fieldLabel, fieldName: f[1].fieldAPIName, type: f[1].fieldType });
                    }
                    fields.push(f[1].fieldAPIName);
                }
                columns.push({ type: 'action', typeAttributes: { rowActions: actions } });
                console.log('columns', columns);
                console.log('fields', fields);
                component.set('v.columns', columns);
                component.set('v.fields', fields);
                this.prepareData(fields, component.get('v.oppId'), component);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error Message',
                        message: errors[0].message,
                        type: 'error',
                        mode: 'sticky'
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },

    prepareData: function (fields, oppID, component) {
        var actionData = component.get("c.getSubmissionList");
        actionData.setParams({
            oppId: oppID,
            fields: fields
        });
        actionData.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var recordsResponce = response.getReturnValue();
                console.log('recordsResponce', recordsResponce);

                component.set('v.data', recordsResponce);
            } else if (state === "ERROR") {
                let errors = response.getError();
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error Message',
                    message: errors[0].message,
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(actionData);
    },

    deleteSubmission: function (component, row) {
        var deleteAction = component.get("c.deleteSubmission");
        var rows = component.get('v.data');
        var rowIndex = rows.indexOf(row);

        deleteAction.setParams({
            recordToDelete: rows[rowIndex].Id
        });
        
        deleteAction.setCallback(this, function (response) {
             var state = response.getState();
             if (state === "SUCCESS") {
                rows.splice(rowIndex, 1);
                component.set('v.data', rows);
                
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Success!",
                    message: "The record has been deleted successfully.",
                    type: 'success',
                });
                toastEvent.fire();
            } else if (state === "ERROR") {
                let errors = response.getError();
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error Message',
                    message: errors[0].message,
                    type: 'error',
                    mode: 'sticky'
                });
                toastEvent.fire();
            }
       });
       $A.enqueueAction(deleteAction);
    }
})