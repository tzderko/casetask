({
    init: function (component, event, helper) {
        helper.prepareColumns(component);
    },
   
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'edit':
                console.log('edit');
                break;
            case 'delete':
                console.log('delete in controller');           
                helper.deleteSubmission(component, row);
                break;
            case 'sendToBank' : 
                console.log('send submission');
                break;
        }
    },





    // ---------------------modal actions------------------------------ 
    createSubmission: function (component, event, helper) {
        component.set("v.isOpenNew", true);
    },

    closeModel: function (component, event, helper) {
        component.set("v.isOpenNew", false);
    },

    onSuccess : function(component, event, helper) {
        let eventL = component.getEvent("lightningEvent");
        let toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        component.set("v.isOpenNew", false);
        eventL.fire();
        toastEvent.fire();
    },

    onSubmit : function(component, event, helper) {
      
    }
    // ---------------------modal actions------------------------------
})