({
    getColumns : function(component) {        
        var columns = [];
        var fields = [];
        var action = component.get("c.queryColumns");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respData = response.getReturnValue();
                for (let fieldValue of Object.entries(respData)) {
                    fields.push(fieldValue[1].FieldName__c);
                    if(fieldValue[1].FieldName__c === 'Account.Name'){
                        fieldValue[1].FieldName__c = 'Account';
                    }
                    columns.push({label: fieldValue[1].Label__c, 
                                  sortable: fieldValue[1].Sortable__c, 
                                  fieldName: fieldValue[1].FieldName__c, 
                                  type:  fieldValue[1].Type__c,  
                                  editable: fieldValue[1].Editable__c});
                }
                component.set('v.fields', fields);
                component.set('v.columns', columns);
                this.prepareData(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.fireToast(component, errors[0].message, 'error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    prepareData : function (component){
        
        var sortBy = component.get("v.sortedBy"); 
        var sortOrder = component.get("v.sortedDirection");
        var pageSize = component.get("v.selectedPageSize");
        var pageNumber = component.get("v.currentPageNumber");
        var status = component.get('v.parentStatus');
        var account = component.get('v.parentAccount');
        var description = component.get('v.parentDescription');
        var fields = component.get('v.fields');

        var action = component.get("c.searchCase");
        this.showSpinner(component);

        action.setParams({
            fields: fields,
            status: status,
            accountName : account,
            description: description,
            pageSize: pageSize,
            pageNumber: pageNumber, 
            sortBy: sortBy,
            sortOrder : sortOrder
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respData = response.getReturnValue();
                if(respData.result.length === 0){
                    this.fireToast(component,'There no any record to display', 'error');
                }else{
                    for (let obj of Object.entries(respData.result)) {
                        if(obj[1].Account){
                            obj[1].Account = obj[1].Account.Name;
                        }  
                    }
                    
                    component.set("v.cases", respData.result );
                    component.set("v.cases", respData.result );
                    component.set("v.totalPages", Math.ceil( respData.totalNumberOfRecords/component.get("v.selectedPageSize")));
                    this.generatePageList(component, pageNumber);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.fireToast(component, errors[0].message, 'error');
                }
            }
        });
        $A.enqueueAction(action);

    },

    generatePageList : function(component, pageNumber ){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        
        if(totalPages > 1){
            if(totalPages <= 10){
                var counter = 1;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(1, 2, 3, 4, 5);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }
        this.hideSpinner(component);
        component.set("v.pageList", pageList);
    },

    updateSelectedCases: function(component, event){
        console.log('in close helper');
        
        var ids = component.get("v.caseIdsToClose");
        var action = component.get("c.updateCases");

        this.showSpinner(component);
        
        if(ids.length <= 0){
            this.fireToast(component, 'Please select Cases to update', 'error');
        }else{
            action.setParams({
                ids: ids
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {    
                    this.fireToast(component, 'All selected Cases was updated successfully!', 'success');
                    this.prepareData(component);
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.fireToast(component, errors[0].message, 'error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    getSelectedRowIds :  function(component, event){
        var selectedRows = event.getParam('selectedRows');
        var caseIds= [];
        for (let caseObj of Object.entries(selectedRows)) {
            caseIds.push(caseObj[1].Id);
        }
        component.set("v.caseIdsToClose",caseIds);
        console.log('caseIds', caseIds);
    },

    fireToast : function(component, message, type){
        this.hideSpinner(component);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: type,
            mode: 'pester',
            duration: 3000
        });
        toastEvent.fire();
    },

    showSpinner: function(component){
        component.find("Id_spinner").set("v.class" , 'slds-show');
    },

    hideSpinner : function(component){
        component.find("Id_spinner").set("v.class" , 'slds-hide');
    }
})