({
    doInit : function(component, event, helper) {
        helper.getColumns(component,event);
    },

    updateTable : function(component, event, helper){
        helper.prepareData(component,event);
    },

    onNext : function(component, event, helper){
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.prepareData(component,event);
    },

    onPrev : function(component, event, helper){
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.prepareData(component,event);
    },

    onPageClick : function(component, event, helper){
        component.set("v.currentPageNumber", parseInt(event.getSource().get('v.name')));
        helper.prepareData(component,event);
    },

    updateColumnSorting : function(component, event, helper){
        component.set("v.sortedDirection", event.getParam("sortDirection"));
        component.set("v.sortedBy", event.getParam("fieldName") );
        helper.prepareData(component,event);
    },

    onRowSelection: function(component, event, helper) {
        helper.getSelectedRowIds(component, event);
    },

    closeCases : function(component, event, helper) {
        helper.updateSelectedCases(component, event);
    }

})