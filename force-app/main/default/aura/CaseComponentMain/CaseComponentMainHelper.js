({
    populateStatusOptionList : function(component) {
        var options = component.get("v.caseStatusOptions"); 
        var action = component.get("c.uniqueCaseFilterData");

        action.setParams({
            searchField: 'Status',
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respData = response.getReturnValue();
                options.push({ label: '--None--', value: null});
                for (let fieldValue of Object.entries(respData)) {
                    options.push({ label: fieldValue[1], value: fieldValue[1]});
                }
                component.set('v.caseStatusOptions', options);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.fireToast(errors[0].message, component);
                }
            }
        });
        $A.enqueueAction(action);
    },

    populateAccountOptionList : function(component){
        var options = component.get("v.caseAccountOptions"); 
        var action = component.get("c.uniqueCaseFilterData");

        action.setParams({
            searchField: 'Account',
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respData = response.getReturnValue();
                options.push({ label: '--None--', value: null});
                for (let fieldValue of Object.entries(respData)) {
                    options.push({ label: fieldValue[1], value: fieldValue[1]});
                }
                component.set('v.caseAccountOptions', options);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.fireToast(errors[0].message, component);
                }
            }
        });
        $A.enqueueAction(action);
    },

    setParamsPickList : function(component){
        component.set('v.showTableComponent', false);
        var descriptionParam = component.find('desctiptionInp').get('v.value');
        var statusParam = component.find('status').get('v.value');
        var accountParam = component.find('accName').get('v.value');

        if((statusParam && statusParam !== '') || (accountParam && accountParam !== '') || (descriptionParam && descriptionParam !== '')){
            this.setAllParams(component, statusParam, accountParam, descriptionParam);
        }else{
            this.fireToast('Please apply at least one criteria for filter.', component);
        }
    },

    setParamsInput : function(component, event){
        var isEnterKey = event.keyCode === 13;
        var queryTerm = component.find('desctiptionInp').get('v.value');
        var statusParam = component.find('status').get('v.value');
        var accountParam = component.find('accName').get('v.value');

        if (isEnterKey) {
            component.set('v.showTableComponent', false);
            if(queryTerm.length >4 || statusParam || accountParam){
                this.setAllParams(component, statusParam, accountParam, queryTerm);
            }else{
                this.fireToast('Please type at least 5 letters to search.', component);
            }
        }
    },

    setAllParams : function(component, statusValue, accNameValue, descriptionValue){
        component.set('v.selectedStatus', statusValue);
        component.set('v.selectedAccount', accNameValue);
        component.set('v.selectedDescription', descriptionValue);
        component.set('v.showTableComponent', true);
    },

    fireToast : function(message, component){
        component.set('v.showTableComponent', false);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: 'error',
            mode: 'pester',
            duration: 3000
        });
        toastEvent.fire();
    }

})
