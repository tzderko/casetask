({
    doInit : function(component, event, helper) {
        helper.populateStatusOptionList(component);
        helper.populateAccountOptionList(component);
    },

    handleChange : function(component, event, helper) {
        helper.setParamsPickList(component, event);
    },

    searchByInput : function(component, event, helper) {
        helper.setParamsInput(component, event);
    }
})
